'use strict';
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw());

// request date logger
app.use(function(req, res, next){
    var date = new Date();
    var time = {
    day:    date.getDate(),
    month:  date.getMonth(),
    year:   date.getYear(),
    hour:   date.getHours(),
    minute:date.getMinutes(),
    second:date.getSeconds()
    };
    console.log(`request made on ${time.day}/${time.month}/${time.year} at ${time.hour}:${time.minute}:${time.second}`);
    next();
});
// JSON Error Checker
app.use(function(req, res, next){
    var data = "";
    req.on('data', function(chunk){ data += chunk})
    req.on('end', function(){
        try{
            req.jsonBody = JSON.parse(data);
        } catch(err){
            if (err) return res.status(400).json({"error": "Could not decode request: JSON parsing failed"});
        }
        next();
    })
});

app.post('/', function(req, res){
    var response = {};
    var a = {};
    req = req.jsonBody["payload"];
    req = req.filter( i => i.episodeCount >= 1);
        response["response"] = req.filter(
            i => i.episodeCount > 0
        ).filter(
            a => a.drm == true
        ).map(
            i => a = {
                image: i.image.showImage,
                slug: i.slug,
                title: i.title
            }
        );
    res.send(response);
});

app.listen(port, function(){
    console.log('server up on port', port)
});